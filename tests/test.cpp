#include "pch.h"
#include "../main/Trit.h"
#include "../main/TritSet.h"
#include <iostream>

Trit::Trit() {
    value = Unknown;
}
Trit::Trit(trit inValue) {
    value = inValue;
};

Trit::operator trit() const {
    return value;
};



Trit Trit::operator &(const Trit& rTrit) const {
    if (value == False || rTrit.value == False)
        return False;
    else if (value == True && rTrit.value == True)
        return True;
    else
        return Unknown;
};
Trit Trit::operator &(const trit& rtrit) const {
    if (value == False || rtrit == False)
        return False;
    else if (value == True && rtrit == True)
        return True;
    else
        return Unknown;
};
Trit operator &(const trit& ltrit, const Trit& rTrit) {
    if (ltrit == False || rTrit.value == False)
        return False;
    else if (ltrit == True && rTrit.value == True)
        return True;
    else
        return Unknown;

};


Trit Trit::operator |(const Trit& rTrit) const {
    if (value == True || rTrit.value == True)
        return True;
    else if (value == False && rTrit.value == False)
        return False;
    else
        return Unknown;
};
Trit Trit::operator |(const trit& rtrit) const {
    if (value == True || rtrit == True)
        return True;
    else if (value == False && rtrit == False)
        return False;
    else
        return Unknown;
};
Trit operator |(const trit& ltrit, const Trit& rTrit) {
    if (ltrit == True || rTrit.value == True)
        return True;
    else if (ltrit == False && rTrit.value == False)
        return False;
    else
        return Unknown;
};


bool Trit::operator ==(const Trit& rTrit) const {
    if (value == rTrit.value)
        return true;
    else
        return false;
};
bool Trit::operator ==(const trit& rtrit) const {
    if (value == rtrit)
        return true;
    else
        return false;
};
bool operator ==(const trit& ltrit, const Trit& rTrit) {
    if (ltrit == rTrit.value)
        return true;
    else
        return false;
};


Trit Trit::operator !() const {
    if (value == True)
        return False;
    else if (value == False)
        return True;
    else
        return Unknown;
};


std::ostream& operator<<(std::ostream& stream, Trit trit) {
    if (trit.value == False)
        stream << "False";
    else if (trit.value == True)
        stream << "True";
    else
        stream << "Unknown";
    return stream;
};

Trit TritSet::getTrit(size_t index) const {
    if (index > sizeT) {
        Trit someTrit;
        return someTrit;
    }
    else {
        size_t numberBox = 2 * index / (8 * sizeof(uint32_t));
        size_t numberBitInBox = 2 * index - numberBox * 8 * sizeof(uint32_t);

        uint32_t tmp = set[numberBox];
        tmp = ((tmp << numberBitInBox) >> (8 * sizeof(uint32_t) - 2));
        if (tmp == 0) {
            Trit someTrit(Unknown);
            return someTrit;
        }
        else if (tmp == 2) {
            Trit someTrit(False);
            return someTrit;
        }
        else if (tmp == 1) {
            Trit someTrit(True);
            return someTrit;
        }
    }
};


TritSet::TritSet(size_t trit_number) {
    firstSize = trit_number;
    sizeT = trit_number;
    for (int i = 0; i <= capacity(); i++)
        set.push_back(0);
};

TritSet::TritSet(const TritSet& other) {
    this->set = other.set;
    this->sizeT = other.sizeT;
    this->firstSize = other.firstSize;
};

size_t TritSet::capacity() const {
    return (sizeT * 2 - 1) / (8 * sizeof(uint32_t)) + 1;
};


Trit TritSet::operator[] (size_t index) const {
    if (index > sizeT) {
        Trit out;
        return out;
    }
    return getTrit(index);
};

TritSet::TritProxy TritSet::operator[] (size_t index) {
    TritProxy proxy(*this, index);
    return proxy;
};


TritSet::TritProxy::TritProxy(TritSet& tritSet, const size_t index) : TrSet(tritSet), frame(index) {};
TritSet::TritProxy::operator Trit() const {
    return TrSet.getTrit(frame);
};

void TritSet::setTrit(size_t index, trit value) {
    size_t numberBox = 2 * index / (8 * sizeof(uint32_t));
    size_t numberBitInBox = 2 * index - numberBox * 8 * sizeof(uint32_t);

    pair<uint32_t, uint32_t> bits;

    bits.first = 3; // 00...0011
    bits.first = ~(bits.first << sizeof(uint32_t) * 8 - numberBitInBox - 2);

    bits.second = 0; //unknown 000...000
    if (value == True) {
        bits.second = 1;//00...0001
        bits.second = bits.second << sizeof(uint32_t) * 8 - numberBitInBox - 2;
    }
    else if (value == False) {
        bits.second = 2; //00...0010
        bits.second = bits.second << sizeof(uint32_t) * 8 - numberBitInBox - 2;
    }
    set[numberBox] = (set[numberBox] & bits.first) | bits.second;
};
void TritSet::getNewPlace(size_t index) {
    size_t num = ((index * 2 - 1) / (8 * sizeof(uint32_t)) + 1) - capacity();
    sizeT = index;
    for (int i = 0; i < num; i++)
        set.push_back(0);
};


TritSet::TritProxy& TritSet::TritProxy::operator =(trit rTrit) {
    if (frame > TrSet.sizeT) {
        if (rTrit == Unknown)
            return *this;
        else {
            TrSet.getNewPlace(frame);
            TrSet.setTrit(frame, rTrit);
            return *this;
        }
    }
    else {
        TrSet.setTrit(frame, rTrit);
        return *this;
    }
};
TritSet::TritProxy& TritSet::TritProxy::operator =(Trit rTrit) {
    return (*this = (trit)rTrit);
};
TritSet::TritProxy& TritSet::TritProxy::operator =(TritSet::TritProxy proxy) {
    Trit nTrit = proxy;
    return (*this = (trit)nTrit);
};


bool TritSet::TritProxy::operator ==(const Trit& rTrit) const {
    Trit lTrit = *this;
    return lTrit == rTrit;
};

const Trit TritSet::TritProxy::operator &(const Trit& rTrit) const {
    Trit lTrit = *this;
    return lTrit & rTrit;
};
const Trit TritSet::TritProxy::operator &(const TritSet::TritProxy& proxy) const {
    Trit lTrit = *this;
    Trit rTrit = proxy;
    return lTrit & rTrit;
};
const Trit operator &(const Trit& lTrit, const TritSet::TritProxy& proxy) {
    Trit rTrit = proxy;
    return lTrit & rTrit;
};

const Trit TritSet::TritProxy::operator |(const Trit& rTrit) const {
    Trit lTrit = *this;
    return lTrit | rTrit;
};
const Trit TritSet::TritProxy::operator |(const TritSet::TritProxy& proxy) const {
    Trit lTrit = *this;
    Trit rTrit = proxy;
    return lTrit | rTrit;
};
const Trit operator |(const Trit& lTrit, const TritSet::TritProxy& proxy) {
    Trit rTrit = proxy;
    return lTrit | rTrit;
};

const Trit TritSet::TritProxy::operator !() {
    Trit nTrit = *this;
    return !nTrit;
};

ostream& operator <<(ostream& stream, TritSet::TritProxy proxy) {
    Trit out = proxy;
    stream << out;
    return stream;
};

TritSet& TritSet::operator =(TritSet rTrit) {
    set.clear();
    set = rTrit.set;
    sizeT = rTrit.sizeT;

    return *this;
};
const TritSet TritSet::operator &(TritSet rTrit) {
    size_t minSize, maxSize;
    if (sizeT >= rTrit.sizeT) {
        minSize = rTrit.sizeT;
        maxSize = sizeT;
    }
    else {
        minSize = sizeT;
        maxSize = rTrit.sizeT;
    }

    TritSet outSet(maxSize);

    for (int i = 0; i <= minSize; i++)
        outSet[i] = getTrit(i) & rTrit[i];
    for (int i = minSize + 1; i <= maxSize; i++)
        outSet[i] = outSet[i] & rTrit[i];

    return outSet;
};
const TritSet TritSet::operator |(TritSet rTrit) {
    size_t minSize, maxSize;
    if (sizeT >= rTrit.sizeT) {
        minSize = rTrit.sizeT;
        maxSize = sizeT;
    }
    else {
        minSize = sizeT;
        maxSize = rTrit.sizeT;
    }

    TritSet outSet(maxSize);

    for (int i = 0; i <= minSize; i++)
        outSet[i] = getTrit(i) | rTrit[i];
    for (int i = minSize + 1; i <= maxSize; i++)
        outSet[i] = outSet[i] | rTrit[i];

    return outSet;
};
const TritSet TritSet::operator !() {
    TritSet outSet(sizeT);
    for (int i = 0; i <= sizeT; i++)
        outSet[i] = !getTrit(i);
    return outSet;
};
TritSet& TritSet::operator &=(TritSet rTrit) {
    *this = *this & rTrit;
    return *this;
};
TritSet& TritSet::operator |=(TritSet rTrit) {
    *this = *this | rTrit;
    return *this;
};
TritSet& TritSet::operator !=(TritSet rTrit) {
    *this = !(*this);
    return *this;
};

size_t TritSet::getLast() const {
    size_t last = 0;
    for (size_t i = 0; i < sizeT; i++)
        if (getTrit(i) != Unknown)
            last = i;
    return last;
};

void TritSet::shrink() {
    size_t newLast, lastTrit = getLast();

    if (lastTrit > firstSize - 1)
        newLast = lastTrit + 1;
    else
        newLast = firstSize;
    size_t num = capacity() - ((newLast * 2 - 1) / (8 * sizeof(uint32_t)) + 1);
    for (size_t i = 0; i < num; i++)
        set.pop_back();
    sizeT = newLast;
};

void TritSet::trim(size_t lastIndex) {
    size_t num = capacity() - ((lastIndex * 2 - 1) / (8 * sizeof(uint32_t)) + 1);
    for (size_t i = 0; i < num; i++)
        set.pop_back();
    sizeT = lastIndex;
};

size_t TritSet::length() const {
    return getLast() + 1;
};

size_t TritSet::cardinality(Trit value) {
    size_t last = getLast();
    if (last == 0) {
        if (value == Unknown)
            return sizeT;
        else
            return 0;
    }
    else {
        size_t count = 0;
        for (size_t i = 0; i < last + 1; i++)
            if (getTrit(i) == value)
                count++;
        return count;
    }
};

TEST(TritSet, capacity) {
    TritSet set(1);
    size_t allocLenght = set.capacity();
    ASSERT_TRUE(allocLenght == 1);

    set[300] = True;
    allocLenght = set.capacity();
    ASSERT_TRUE(allocLenght == (300 * 2 - 1) / (8 * sizeof(uint32_t)) + 1);

    set[500] = Unknown;
    size_t allocLenghtNew = set.capacity();
    ASSERT_TRUE(allocLenght == allocLenghtNew);
}

TEST(TritSet, shrink) {
    TritSet set(100);
    size_t allocLenght = set.capacity();
    set[50] = True;
    ASSERT_TRUE(allocLenght == set.capacity());
    set.shrink();
    ASSERT_TRUE(allocLenght == set.capacity());

    set[150] = True;
    allocLenght = set.capacity();
    set[150] = Unknown;
    set.shrink();
    ASSERT_TRUE(allocLenght > set.capacity());
    ASSERT_TRUE(set.capacity() == (100 * 2 - 1) / (8 * sizeof(uint32_t)) + 1);
}

TEST(TritSet, length) {
    TritSet set(100);
    set[50] = True;
    ASSERT_TRUE(set.length() == 51);
    set[50] = Unknown;
    ASSERT_TRUE(set.length() == 1);
}

TEST(TritSet, trim) {
    TritSet set(100);
    set[50] = set[51] = True;
    set.trim(51);
    ASSERT_TRUE(set[50] == True);
    ASSERT_TRUE(set[52] == Unknown);
}

TEST(TritSet, cardinality) {
    TritSet set(100);
    set[1] = set[2] = set[3] = True;
    set[4] = set[5] = set[6] = set[9] = False;
    ASSERT_TRUE(set.cardinality(True) == 3);
    ASSERT_TRUE(set.cardinality(False) == 4);
    cout << "\n" << set.cardinality(Unknown) << "\n";
    ASSERT_EQ(set.cardinality(Unknown), 3);
}

TEST(TritSet, operators) {
    TritSet set(100);
    set[0] = False;
    set[1] = Unknown;
    set[2] = True;
    ASSERT_TRUE((set[0] & set[0]) == False); // &
    ASSERT_TRUE((set[0] & set[1]) == False);
    ASSERT_TRUE((set[1] & set[0]) == False);
    ASSERT_TRUE((set[1] & set[2]) == Unknown);
    ASSERT_TRUE((set[2] & set[1]) == Unknown);
    ASSERT_TRUE((set[1] & set[1]) == Unknown);
    ASSERT_TRUE((set[2] & set[2]) == True);
    ASSERT_TRUE((set[2] & set[0]) == False);
    ASSERT_TRUE((set[0] & set[2]) == False);

    ASSERT_TRUE((set[0] | set[0]) == False); // |
    ASSERT_TRUE((set[0] | set[1]) == Unknown);
    ASSERT_TRUE((set[1] | set[0]) == Unknown);
    ASSERT_TRUE((set[1] | set[2]) == True);
    ASSERT_TRUE((set[2] | set[1]) == True);
    ASSERT_TRUE((set[1] | set[1]) == Unknown);
    ASSERT_TRUE((set[2] | set[2]) == True);
    ASSERT_TRUE((set[2] | set[0]) == True);
    ASSERT_TRUE((set[0] | set[2]) == True);

    TritSet setA(100);  // &=
    setA[5] = False;
    setA[6] = True;
    setA[7] = Unknown;
    TritSet setB(200);
    setB[5] = True;
    setB[6] = True;
    setB[7] = True;
    setB[150] = True;
    setB[151] = False;
    setB[152] = Unknown;
    setA &= setB;
    ASSERT_TRUE(setA.capacity() == setB.capacity());
    ASSERT_TRUE(setA[5] == False);
    ASSERT_TRUE(setA[6] == True);
    ASSERT_TRUE(setA[7] == Unknown);
    ASSERT_TRUE(setA[150] == Unknown);
    ASSERT_TRUE(setA[151] == False);
    ASSERT_TRUE(setA[152] == Unknown);

    ASSERT_FALSE(setA[300] == True);

    TritSet setA1(100);  // |=
    setA1[5] = False;
    setA1[6] = True;
    setA1[7] = Unknown;
    TritSet setB1(200);
    setB1[5] = True;
    setB1[6] = True;
    setB1[7] = True;
    setB1[150] = True;
    setB1[151] = False;
    setB1[152] = Unknown;
    setA1 |= setB1;
    ASSERT_TRUE(setA1.capacity() == setB1.capacity());
    ASSERT_TRUE(setA1[5] == True);
    ASSERT_TRUE(setA1[6] == True);
    ASSERT_TRUE(setA1[7] == True);
    ASSERT_TRUE(setA1[150] == True);
    ASSERT_TRUE(setA1[151] == Unknown);
    ASSERT_TRUE(setA1[102] == Unknown);

    TritSet setC(100);  // !
    setC[0] = True;
    setC[1] = Unknown;
    setC[2] = False;
    TritSet setNoSetC = !setC;
    ASSERT_TRUE(setNoSetC[0] == False);
    ASSERT_TRUE(setNoSetC[1] == Unknown);
    ASSERT_TRUE(setNoSetC[2] == True);

}
